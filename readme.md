## NewsApp

![newsapp](/uploads/6dfbe4febce80e2951175c69ebb08d51/newsapp.png)
## Title of the project - NewsApp
**Who did :** Hi, I am Sagar. I created this NewsApp.

**Role of Sagar:** Handled both Frontend and backend

**Description:** NewsApp understands your preferences over time, presenting a dynamic selection of articles based on your interests, ensuring you never miss out on the stories that resonate with you.

**Technical Functionality:** It doesn't have a data store, used public data. 

**Security:** It doesn't have any Security features as of now , will be added soon.

**Challenges faced:** Difficulty to find the proper data for api, difficult to publish it in Azure.Faced problem for CORS(Cross Origin Resource Sharing).

**How did i solve this:** Considered exploring multiple News APIs to find the one that aligns with the apps requirement. Configured the server to include necessary CORS headers. 

**What is left:** Security for the application has to added. Multi factor authentication will be implementing using Microsoft Entra ID. 

**Technology and Tools Used:** HTML,CSS,JAVASCRIPT,Microsoft Entra ID.
****
**Conclusion:** NewsApp is not just a news app; it's a dynamic and personalized gateway to the latest information tailored to your unique interests. With powerful search capabilities, real-time updates, and a user-friendly interface, we've crafted an immersive experience that puts you in control of the news that matters most to you.

**References:** Here are some references that can guide you through different aspects of developing and improving your news app:

News API: A popular API that provides a simple HTTP REST API for searching and retrieving live articles from all over the web.
Microsoft Azure Documentation:

Azure Documentation: The official documentation for Microsoft Azure provides detailed guides and tutorials on deploying applications, setting up Azure services, and more.
Cross-Origin Resource Sharing (CORS):

MDN Web Docs - Cross-Origin Resource Sharing (CORS): Mozilla's documentation on CORS explains the concept and provides guidance on how to handle cross-origin requests.
Microsoft Azure Multi-Factor Authentication:

Azure Multi-Factor Authentication Documentation: A guide to getting started with Multi-Factor Authentication in Azure Active Directory.



